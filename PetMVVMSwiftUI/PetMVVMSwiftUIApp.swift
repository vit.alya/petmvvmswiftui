//
//  PetMVVMSwiftUIApp.swift
//  PetMVVMSwiftUI
//
//  Created by Vity Kinchin on 02.08.2023.
//

import SwiftUI

@main
struct PetMVVMSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
